#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "timex.h"
#include "ztimer.h"
#include "shell.h"
#include "shell_commands.h"

#include "board.h"
#include "periph/gpio.h"

#include "nimble_scanner.h"
#include "nimble_scanlist.h"

/* default scan duration (1s) */
#define DEFAULT_DURATION_MS        (1 * MS_PER_SEC)

static char my_stack[THREAD_STACKSIZE_DEFAULT];
int8_t is_scanning = 0;

void* scan_ble_thread(void* arg) {
    (void) arg;
    is_scanning = 1;
    LED0_TOGGLE;
    uint32_t timeout = DEFAULT_DURATION_MS;

    nimble_scanlist_clear();
    printf("Scanning for %"PRIu32" ms now ...", timeout);
    nimble_scanner_set_scan_duration(timeout);
    nimble_scanner_start();
    ztimer_sleep(ZTIMER_MSEC, timeout);
    puts(" done\n\nResults:");
    nimble_scanlist_print();
//    puts("");
    LED0_TOGGLE;
    is_scanning = 0;

    return 0;
}


void scan_ble(void *arg) {
    (void) arg;
    if(!is_scanning)
        thread_create(my_stack, sizeof(my_stack),
                      THREAD_PRIORITY_MAIN-1, THREAD_CREATE_STACKTEST,
                      scan_ble_thread, NULL, "Scanning thread");
    cortexm_isr_end();
    return;
}


int main(void) {
    gpio_init_int(BTN0_PIN, GPIO_IN_PU, GPIO_FALLING, scan_ble, NULL);

    puts("NimBLE Scanner Example Application");
    puts("Push btn1 for scan");

    struct ble_gap_disc_params scan_params = {
        .itvl = BLE_GAP_LIM_DISC_SCAN_INT,
        .window = BLE_GAP_LIM_DISC_SCAN_WINDOW,
        .filter_policy = 0,                         /* don't use */
        .limited = 0,                               /* no limited discovery */
        .passive = 0,                               /* no passive scanning */
        .filter_duplicates = 0,                    /* no duplicate filtering */
    };

    /* initialize the nimble scanner */
    nimble_scanlist_init();
    nimble_scanner_init(&scan_params, nimble_scanlist_update);

    while (1) {
        LED1_TOGGLE;
        ztimer_sleep(ZTIMER_MSEC, 100);
    }

    return 0;
}
