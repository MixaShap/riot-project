#include "nimble_9159.h"

#include <stdio.h>
#include <stdlib.h>

#include "board.h"

#include "ztimer.h"

#include "net/gnrc/rpl.h"

#include "nimble_riot.h"
#include "nimble_netif.h"
#include "nimble_netif_conn.h"
#include "net/bluetil/ad.h"
#include "net/bluetil/addr.h"

#include "nimble_scanlist.h"
#include "nimble_scanner.h"

/* default scan duration (1s) */
#define DEFAULT_DURATION_MS        (1 * MS_PER_SEC)
#define DEFAULT_CONN_TIMEOUT       (500U)      /* 500ms */

#define ADV_NAME    "MIEM_nrf52"
#define ADV_NAME_PREFIX_LENGTH 4

static volatile int can_connect = 0;
static mutex_t _lock = MUTEX_INIT;

static char my_stack[THREAD_STACKSIZE_DEFAULT];

static void adv(void);

static void _print_evt(const char *msg, int handle, const uint8_t *addr)
{
    printf("event: handle %i -> %s (", handle, msg);
    bluetil_addr_print(addr);
    puts(")");
}

static int _conn_close(nimble_netif_conn_t *conn, int handle, void *arg)
{
    (void) conn;
    (void) arg;
    nimble_netif_close(handle);
    return 0;
}

static void _on_ble_evt(int handle, nimble_netif_event_t event,
                        const uint8_t *addr)
{
    switch (event) {
        case NIMBLE_NETIF_CONNECTED_MASTER:
            _print_evt("CONNECTED as MASTER", handle, addr);
            break;
        case NIMBLE_NETIF_CONNECTED_SLAVE:
            _print_evt("CONNECTED as SLAVE", handle, addr);
            LED0_ON;
            gnrc_rpl_send_DIS(NULL, (ipv6_addr_t *) &ipv6_addr_all_rpl_nodes, NULL, 0);
            puts("9159: send a DIS");
            mutex_lock(&_lock);
            can_connect = 1;
            mutex_unlock(&_lock);
            break;
        case NIMBLE_NETIF_CLOSED_MASTER:
        case NIMBLE_NETIF_ABORT_MASTER:
            _print_evt("CONNECTION CLOSED by myself (SLAVE)", handle, addr);
            break;
        case NIMBLE_NETIF_CLOSED_SLAVE:
        case NIMBLE_NETIF_ABORT_SLAVE:
            LED0_OFF;
            _print_evt("CONNECTION CLOSED by MASTER", handle, addr);
            // close all connections with slaves and enable advertising
            mutex_lock(&_lock);
            can_connect = 0;
            mutex_unlock(&_lock);
            nimble_netif_conn_foreach(NIMBLE_NETIF_L2CAP_CONNECTED,
                                  _conn_close, NULL);
            adv();
            break;
        case NIMBLE_NETIF_CONN_UPDATED:
        default:
            /* do nothing */
            break;
    }
}

static void adv(void) {
    bluetil_ad_t ad;
    int res;
    uint8_t buf[BLE_HS_ADV_MAX_SZ];
    const struct ble_gap_adv_params _adv_params = {
            .conn_mode = BLE_GAP_CONN_MODE_UND,
            .disc_mode = BLE_GAP_DISC_MODE_LTD,
            .itvl_min = BLE_GAP_ADV_FAST_INTERVAL2_MIN,
            .itvl_max = BLE_GAP_ADV_FAST_INTERVAL2_MAX,
    };

    /* make sure no advertising is in progress */
    if (nimble_netif_conn_is_adv()) {
        puts("err: advertising already in progress");
        return;
    }

    /* build advertising data */
    res = bluetil_ad_init_with_flags(&ad, buf, BLE_HS_ADV_MAX_SZ,
                                     BLUETIL_AD_FLAGS_DEFAULT);
    assert(res == BLUETIL_AD_OK);
    uint16_t ipss = BLE_GATT_SVC_IPSS;
    res = bluetil_ad_add(&ad, BLE_GAP_AD_UUID16_INCOMP, &ipss, sizeof(ipss));
    assert(res == BLUETIL_AD_OK);
    res = bluetil_ad_add(&ad, BLE_GAP_AD_NAME, ADV_NAME, strlen(ADV_NAME));
    if (res != BLUETIL_AD_OK) {
        puts("err: the given name is too long");
        return;
    }

    /* start listening for incoming connections */
    res = nimble_netif_accept(ad.buf, ad.pos, &_adv_params);
    if (res != 0) {
        printf("err: unable to start advertising (%i)\n", res);
    } else {
        printf("success: advertising this node as '%s'\n", ADV_NAME);
    }
}

void* scan_ble_thread(void* arg) {
    (void) arg;
    while (1) {
        ztimer_sleep(ZTIMER_MSEC, 10*1000);
        
        mutex_lock(&_lock);
        if(can_connect){
            mutex_unlock(&_lock);
        
            LED1_ON;
            uint32_t timeout = DEFAULT_DURATION_MS;

            nimble_scanlist_clear();
            printf("Scanning for %" PRIu32 " ms now ...\n", timeout);
            nimble_scanner_set_scan_duration(timeout);
            nimble_scanner_start();
            ztimer_sleep(ZTIMER_MSEC, timeout);
            puts("\nResults:");

            /** Iterate through all scan results **/
            int16_t max_rssi = INT16_MIN;
            nimble_scanlist_entry_t *best = NULL;
            nimble_scanlist_entry_t *e = nimble_scanlist_get_next(NULL);
            while (e) {
                char name[(BLE_ADV_PDU_LEN + 1)] = {0};
                bluetil_ad_t ad = BLUETIL_AD_INIT(e->ad, e->ad_len, e->ad_len);
                int res = bluetil_ad_find_str(&ad, BLE_GAP_AD_NAME, name, sizeof(name));
                if (res == BLUETIL_AD_OK) {
                    if ((e->type == BLE_HCI_ADV_TYPE_ADV_IND) & (strncmp(name, ADV_NAME, ADV_NAME_PREFIX_LENGTH) == 0)) {
                        uint8_t addrn[BLE_ADDR_LEN];
                        bluetil_addr_swapped_cp(e->addr.val, addrn);
                        
                        printf("%s\t\n", name);
                        bluetil_addr_print(addrn);
                        printf("\tRSSI: %i\n", e->last_rssi);
                        
                        if(e->last_rssi > max_rssi){
                            max_rssi = e->last_rssi;
                            best = e;
                        }
                    }
                }
                e = nimble_scanlist_get_next(e);
            }

            nimble_scanner_stop();
            printf("Scan end\n");
            LED1_OFF;
            
            mutex_lock(&_lock);
            if(can_connect){
                mutex_unlock(&_lock);
                if(best){
                    //connect_ble(sensors[max_rssi_id]);
                    int res = nimble_netif_connect(&best->addr, NULL, DEFAULT_CONN_TIMEOUT);
                    if (res < 0) {
                        printf("err: unable to trigger connection sequence (%i) with", res);
                    }
                    printf("Successfully connected to");
                    uint8_t addrn[BLE_ADDR_LEN];
                    bluetil_addr_swapped_cp(best->addr.val, addrn);
                    bluetil_addr_print(addrn);
                    puts("");
                }
            } else {
                mutex_unlock(&_lock);
            }
        } else {
            mutex_unlock(&_lock);
        }
    }

    return 0;
}


void nimble_9159_init(int is_master) {
    (void) is_master;
    
    LED0_OFF;
    LED1_OFF;
    
    /* init RPL on all available interfaces (there is only one in our case */
    gnrc_netif_t *netif = NULL;

    while ((netif = gnrc_netif_iter(netif))) {
        gnrc_rpl_init(netif->pid);
        printf("successfully initialized RPL on interface %d\n", netif->pid);
    }
    
    /* setup the scanning environment */
    struct ble_gap_disc_params scan_params = {
        .itvl = BLE_GAP_LIM_DISC_SCAN_INT,
        .window = BLE_GAP_LIM_DISC_SCAN_WINDOW,
        .filter_policy = 0,                         /* don't use */
        .limited = 0,                               /* no limited discovery */
        .passive = 0,                               /* no passive scanning */
        .filter_duplicates = 0,                    /* no duplicate filtering */
    };
    nimble_scanlist_init();
    nimble_scanner_init(&scan_params, nimble_scanlist_update);

    /* register event callback with the netif wrapper */
    nimble_netif_eventcb(_on_ble_evt);
    
    /* start advertising */
    adv();
    
    thread_create(my_stack, sizeof(my_stack),
                      THREAD_PRIORITY_MAIN+1, THREAD_CREATE_STACKTEST,
                      scan_ble_thread, NULL, "Scanning thread");
}