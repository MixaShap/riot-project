# NimBLE Scanner Conn

## About
This application searching sensors with name 'nrf52_MIEM' and 

(ADV_NAME constant in main.c)

## Usage
Push Btn1 for scan. Sensors will be shown

Example output:
```
main(): This is RIOT! (Version: 2021.10)
NimBLE Sensor Finder Application
Push btn1 for scan
Scanning for 1000 ms now ...

Results:
[ 1] nrf52_MIEM C1:E2:B9:42:08:D3
Scan end
```

## Connection to other boards
#### How to:
* Flash scanner_conn to master board
* Flash autoadv to other boards
* Push btn1 on master board
* It will connect to slave board with max RSSI
* Slave board will enable LED3 when master board connected

## Future
Module connects to the found sensors automatically
