//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>

//#include "timex.h"
//#include "ztimer.h"
//#include "shell.h"
//#include "msg.h"
//#include "shell_commands.h"

//#include "board.h"
#include "periph/gpio.h"

#include "net/gnrc.h"
#include "net/gnrc/rpl.h"

#include "nimble_scanner.h"
#include "nimble_scanlist.h"
#include "nimble_netif.h"

#include "net/bluetil/ad.h"
#include "net/bluetil/addr.h"


/* default scan duration (1s) */
#define DEFAULT_DURATION_MS        (1 * MS_PER_SEC)

#define ADV_NAME    "nrf52_MIEM"
#define DEFAULT_CONN_TIMEOUT        (500U)      /* 500ms */

#define GNRC_RPL_DEFAULT_NETIF      6

//#define MAIN_QUEUE_SIZE     (8)
//static msg_t _main_msg_queue[MAIN_QUEUE_SIZE];


static char my_stack[THREAD_STACKSIZE_DEFAULT];
static kernel_pid_t scanning_thread;
//int8_t is_scanning = 0;

nimble_scanlist_entry_t** connections;

char sensors[10][10];
int sensor_rssi[10];


void connect_ble(char* address) {
    /* try if param is an BLE address */
    uint8_t addr_in[BLE_ADDR_LEN];

    nimble_scanner_stop();

    printf("Connecting to %s...\n", address);
    if (bluetil_addr_from_str(addr_in, address) != NULL) {
        ble_addr_t addr = { .type = BLE_ADDR_RANDOM };
        /* NimBLE expects address in little endian, so swap */
        bluetil_addr_swapped_cp(addr_in, addr.val);

        int res = nimble_netif_connect(&addr, NULL, DEFAULT_CONN_TIMEOUT);
        if (res < 0) {
            printf("err: unable to trigger connection sequence (%i)\n", res);
            return;
        }
        printf("Successfully connected to %s...\n", address);
        return;
    }
}


void* scan_ble_thread(void* arg) {
    (void) arg;
    while (1) {
        msg_t msg;
        msg_receive(&msg);
        if (msg.content.value == 2) {
            LED0_TOGGLE;
            uint32_t timeout = DEFAULT_DURATION_MS;

            nimble_scanlist_clear();
            printf("Scanning for %" PRIu32 " ms now ...\n", timeout);
            nimble_scanner_set_scan_duration(timeout);
            nimble_scanner_start();
            ztimer_sleep(ZTIMER_MSEC, timeout);
            puts("\nResults:");

            /** Saving hosts to arrays **/
            int i = 0;
            nimble_scanlist_entry_t *e = nimble_scanlist_get_next(NULL);
            connections[i] = e;
//    i++;
            while (e) {
                char name[(BLE_ADV_PDU_LEN + 1)] = {0};
                bluetil_ad_t ad = BLUETIL_AD_INIT(e->ad, e->ad_len, e->ad_len);
                int res = bluetil_ad_find_str(&ad, BLE_GAP_AD_NAME, name, sizeof(name));
                if (res == BLUETIL_AD_OK) {
                    if ((e->type == BLE_HCI_ADV_TYPE_ADV_IND) & (strcmp(name, ADV_NAME) == 0)) {
                        ble_addr_t *sensor_addr = &e->addr;
                        uint8_t addrn[BLE_ADDR_LEN];
                        bluetil_addr_swapped_cp(sensor_addr->val, addrn);
                        bluetil_addr_sprint(sensors[i], addrn);
                        sensor_rssi[i] = (int) e->last_rssi;

                        connections[i + 1] = e;
                        printf("[%2u] %s %s\tRSSI: %i\n", i, name, sensors[i], sensor_rssi[i]);

                        i++;
                    }
                }
                e = nimble_scanlist_get_next(e);
            }

            printf("Scan end. Found %i devices\n", i);

            int max_rssi = -1000;
            int max_rssi_id = -1;
            for (int sensor_id = 0; sensor_id < i; sensor_id++) {
                if (sensor_rssi[sensor_id] > max_rssi) {
                    max_rssi = sensor_rssi[sensor_id];
                    max_rssi_id = sensor_id;
                }
            }

            if (max_rssi_id != -1) {
                connect_ble(sensors[max_rssi_id]);
            }

            LED0_TOGGLE;
        }
    }

    return 0;
}


void scan_ble(void *arg) {
    (void) arg;

    msg_t msg;
    msg.content.value = 2;
    msg_send(&msg, scanning_thread);

//    if(!is_scanning)
//        thread_create(my_stack, sizeof(my_stack),
//                      THREAD_PRIORITY_MAIN-1, THREAD_CREATE_STACKTEST,
//                      scan_ble_thread, NULL, "Scanning thread");
//    cortexm_isr_end();
    return;
}


//static const shell_command_t shell_commands[] = {
//        { "udp", "send data over UDP and listen on UDP ports", pass },
//        { NULL, NULL, NULL }
//};


int main(void) {
    // for shell
//    msg_init_queue(_main_msg_queue, MAIN_QUEUE_SIZE);

    gpio_init_int(BTN0_PIN, GPIO_IN_PU, GPIO_FALLING, scan_ble, NULL);
    scanning_thread = thread_create(my_stack, sizeof(my_stack),
                      THREAD_PRIORITY_MAIN-1, THREAD_CREATE_STACKTEST,
                      scan_ble_thread, NULL, "Scanning thread");

    puts("NimBLE Sensor Finder Application");
    puts("Push btn1 for scan");

    struct ble_gap_disc_params scan_params = {
        .itvl = BLE_GAP_LIM_DISC_SCAN_INT,
        .window = BLE_GAP_LIM_DISC_SCAN_WINDOW,
        .filter_policy = 0,                         /* don't use */
        .limited = 0,                               /* no limited discovery */
        .passive = 0,                               /* no passive scanning */
        .filter_duplicates = 0,                    /* no duplicate filtering */
    };

    /** initialize the nimble scanner **/
    nimble_scanlist_init();
    nimble_scanner_init(&scan_params, nimble_scanlist_update);


    //// RPL init

//    gnrc_netif_t *netif = gnrc_netif_iter(NULL);
//    kernel_pid_t gnrc_rpl_init(kernel_pid_t if_pid);

    gnrc_rpl_init(6);       // Because ifconfig shows interface no. 6
    printf("Is init? %d\n", gnrc_netif_highlander());
    printf("Amount of interfaces: %d\n", gnrc_netif_numof());
//    gnrc_rpl_dodag_init();
//    gnrc_rpl_dodag_show();


//    puts("All up, running the shell now");
//    char line_buf[SHELL_DEFAULT_BUFSIZE];
//    shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

    while (1) {
        LED1_TOGGLE;
        ztimer_sleep(ZTIMER_MSEC, 100);
    }

    return 0;
}
