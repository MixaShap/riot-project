#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "timex.h"
#include "ztimer.h"
#include "shell.h"
#include "shell_commands.h"
#include "nimble_autoadv.h"
#include "net/bluetil/ad.h"
#include "net/bluetil/addr.h"

#include "nimble_riot.h"
#include "nimble_netif.h"
#include "nimble_netif_conn.h"

#include "board.h"
#include "periph/gpio.h"
#include "periph/pwm.h"
#include "xtimer.h"


#define TX_PIN GPIO_PIN(0, 12)
#define RX_PIN GPIO_PIN(0, 21)


/* default scan duration (1s) */
#define DEFAULT_DURATION_MS        (1 * MS_PER_SEC)
#define NAME    "nrf52_MIEM"


msg_t ir_msg;
kernel_pid_t gpio_pid;


uint32_t imp_start = 0;
uint8_t rec_byte = 0;
uint8_t imp_cnt = 0;


char mac[BLUETIL_ADDR_STRLEN];
char ipv6[BLUETIL_IPV6_IID_STRLEN];

static char send_stack[THREAD_STACKSIZE_DEFAULT];
static char tx_stack[THREAD_STACKSIZE_DEFAULT];
static char rx_stack[THREAD_STACKSIZE_DEFAULT];

int isConnected = 0;

extern void send(char *addr_str, char *port_str, char *data, unsigned int num,
                        unsigned int delay);

extern int udp_cmd(int argc, char **argv);


int toggle_led(int argc, char **argv) {
    if ((argc == 2) && (memcmp(argv[1], "help", 4) == 0)) {
        printf("usage: %s [timeout in ms]\n", argv[0]);
        return 0;
    }

    LED2_TOGGLE;
    return 0;
}


static int _conn_dump(nimble_netif_conn_t *conn, int handle, void *arg) {
    (void)arg;
    (void)handle;

    char str[BLUETIL_ADDR_STRLEN];
    bluetil_addr_sprint(str, conn->addr);
    strcpy(mac, str);

    char tmp[BLUETIL_IPV6_IID_STRLEN];
    bluetil_addr_ipv6_l2ll_sprint(tmp, conn->addr);
    char ip[BLUETIL_ADDR_STRLEN];
    strcpy(ip, &tmp[1]);
    strncpy(ipv6, ip, strlen(ip)-1);

    return 0;
}


void adv(void) {
    bluetil_ad_t ad;
    int res;
    uint8_t buf[BLE_HS_ADV_MAX_SZ];
    const struct ble_gap_adv_params _adv_params = {
            .conn_mode = BLE_GAP_CONN_MODE_UND,
            .disc_mode = BLE_GAP_DISC_MODE_LTD,
            .itvl_min = BLE_GAP_ADV_FAST_INTERVAL2_MIN,
            .itvl_max = BLE_GAP_ADV_FAST_INTERVAL2_MAX,
    };

    /* make sure no advertising is in progress */
    if (nimble_netif_conn_is_adv()) {
        puts("err: advertising already in progress");
        return;
    }

    /* build advertising data */
    res = bluetil_ad_init_with_flags(&ad, buf, BLE_HS_ADV_MAX_SZ,
                                     BLUETIL_AD_FLAGS_DEFAULT);
    assert(res == BLUETIL_AD_OK);
    uint16_t ipss = BLE_GATT_SVC_IPSS;
    res = bluetil_ad_add(&ad, BLE_GAP_AD_UUID16_INCOMP, &ipss, sizeof(ipss));
    assert(res == BLUETIL_AD_OK);
    res = bluetil_ad_add(&ad, BLE_GAP_AD_NAME, NAME, strlen(NAME));
    if (res != BLUETIL_AD_OK) {
        puts("err: the given name is too long");
        return;
    }

    /* start listening for incoming connections */
    res = nimble_netif_accept(ad.buf, ad.pos, &_adv_params);
    if (res != 0) {
        printf("err: unable to start advertising (%i)\n", res);
    } else {
        printf("success: advertising this node as '%s'\n", NAME);
    }
}


void ble_info(void) {
    uint8_t own_addr[BLE_ADDR_LEN];
    uint8_t tmp_addr[BLE_ADDR_LEN];
    ble_hs_id_copy_addr(nimble_riot_own_addr_type, tmp_addr, NULL);
    bluetil_addr_swapped_cp(tmp_addr, own_addr);
    printf("Own Address: ");
    bluetil_addr_print(own_addr);

    unsigned active = nimble_netif_conn_count(NIMBLE_NETIF_L2CAP_CONNECTED);
    printf("\nConnections: %u\n", active);
    if (active > 0) {
        nimble_netif_conn_foreach(NIMBLE_NETIF_L2CAP_CONNECTED,
                                  _conn_dump, NULL);
    }
    return;
}


static void _on_ble_evt(int handle, nimble_netif_event_t event,
                        const uint8_t *addr)
{
    (void) addr;
    (void) handle;
    switch (event) {
        case NIMBLE_NETIF_CONNECTED_SLAVE:
            LED2_TOGGLE;
            puts("Connected as slave!");
            ble_info();
            puts(mac);
            puts(ipv6);
            isConnected = 1;
            break;
        default:
            /* do nothing */
            break;
    }
}


void* send_stuff(void* arg) {
    (void) arg;
    puts("sending...");
    uint32_t num = 1;
    uint32_t delay = 1;
    send(ipv6, "1234", "Hello from other side", num, delay);
    return 0;
}


void btn_send(void *arg) {
    (void) arg;
    if(isConnected){
        thread_create(send_stack, sizeof(send_stack),
                      THREAD_PRIORITY_MAIN-1, THREAD_CREATE_STACKTEST,
                      send_stuff, NULL, "Sending thread");
    }
    return;
}


static const shell_command_t shell_commands[] = {
        { "udp", "send data over UDP and listen on UDP ports", udp_cmd },
        { "tgl", "toggle led 3", toggle_led },
        { NULL, NULL, NULL }
};


void* tx_blink(void* arg) {
    (void) arg;

    for(uint8_t i = 0; i < pwm_channels(PWM_DEV(0)); i++){
        pwm_set(PWM_DEV(0), i, 210);
    }

    uint8_t byte = 'A';
    while(true){
        uint8_t byte_tx = byte;
        // NEC IR-like (no inverse byte transmission yet)
        pwm_set(PWM_DEV(0), 0, 105);
        xtimer_usleep(9000);
        pwm_set(PWM_DEV(0), 0, 0);
        xtimer_usleep(4500);
        for(int i = 0; i < 8; i++){
            pwm_set(PWM_DEV(0), 0, 105);
            xtimer_usleep(560);
            pwm_set(PWM_DEV(0), 0, 0);
            xtimer_usleep( (byte_tx & 1) ? 1700 : 560 );
            byte_tx >>= 1;
        }
        pwm_set(PWM_DEV(0), 0, 105);
        xtimer_usleep(560);
        pwm_set(PWM_DEV(0), 0, 0);

        // pause for 100 ms
        xtimer_msleep(100);
    }
}


void gpio_handler(void *arg){
    (void) arg;

    if(gpio_read(RX_PIN)){
        // start of the impulse
        imp_start = xtimer_now_usec();
    } else {
        // impulse end - count length and act accordingly
        uint32_t imp_end = xtimer_now_usec();
        imp_end -= imp_start;

        if(imp_end > 4000) {
            // resync
            rec_byte = 0;
            imp_cnt = 0;
        } else {
            if(imp_cnt < 8 && imp_end > 1100){
                rec_byte |= (1 << imp_cnt);
            }
            imp_cnt++;

            if(imp_cnt == 8){
                // full byte
                ir_msg.content.value = rec_byte;
                msg_send(&ir_msg, gpio_pid);
            }
        }
    }
}


void* receiver(void *arg) {
    (void) arg;

    gpio_pid = thread_getpid();

    while(true){
        msg_t m;
        int res = xtimer_msg_receive_timeout(&m, 150*1000); // 150 ms must be enough


        if(res < 0 || m.content.value != 'A'){
            gpio_set(LED3_PIN);
            if (isConnected)
                send(ipv6, "1234", "Line crossedW", 1, 1);
            puts("Line crossed!");
        } else {
            gpio_clear(LED3_PIN);
//            puts("Line free!");
        }
    }
}

int main(void) {
    gpio_init_int(RX_PIN, GPIO_IN, GPIO_BOTH , gpio_handler, NULL);
    gpio_init_int(BTN0_PIN, GPIO_IN_PU, GPIO_FALLING, btn_send, NULL);

    puts("NimBLE Autoadv Application");
    printf("Look for bluetooth %s on your phone\n", NAME);
    puts("\n\nPrint tgl for toggle led 3");
    puts("Push btn1 to send msg");

    adv();
    nimble_netif_eventcb(_on_ble_evt);

    /* Transmitter thread */
    pwm_init(PWM_DEV(0), PWM_RIGHT, 38000, 210);
    thread_create(tx_stack, sizeof(tx_stack),
                  THREAD_PRIORITY_MAIN-1, THREAD_CREATE_STACKTEST,
                  tx_blink, NULL, "Transmitter thread");

    /* Receiver thread */
    thread_create(rx_stack, sizeof(rx_stack),
                  THREAD_PRIORITY_MAIN-1, THREAD_CREATE_STACKTEST,
                  receiver, NULL, "Receiver thread");

    /* start shell */
    char line_buf[SHELL_DEFAULT_BUFSIZE];
    shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

    return 0;
}
