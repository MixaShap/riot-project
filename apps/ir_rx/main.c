#include <stdio.h>

#include "periph/gpio.h"
#include "xtimer.h"

#define INPUT_PIN GPIO_PIN(0, 12)

msg_t ir_msg;
kernel_pid_t main_pid;

// TODO: define a structure for all of this, pass it to gpio_handler
uint32_t imp_start = 0;
uint8_t rec_byte = 0;
uint8_t imp_cnt = 0;

void gpio_handler(void *arg){
    (void) arg;
    
    if( gpio_read(INPUT_PIN) ){
        // start of the impulse
        imp_start = xtimer_now_usec();
    } else {
        // impulse end - count length and act accordingly
        uint32_t imp_end = xtimer_now_usec();
        imp_end -= imp_start;
        
        if(imp_end > 4000){
            // resync
            rec_byte = 0;
            imp_cnt = 0;
        } else {
            if(imp_cnt < 8 && imp_end > 1100){
                rec_byte |= (1 << imp_cnt);
            }
            imp_cnt++;
            
            if(imp_cnt == 8){
                // full byte
                ir_msg.content.value = rec_byte;
                msg_send(&ir_msg, main_pid);
            }
        }
    }
}

int main(void)
{
    main_pid = thread_getpid();
    gpio_init_int( INPUT_PIN, GPIO_IN, GPIO_BOTH , gpio_handler, NULL );

    while(true){
        msg_t m;
        int res = xtimer_msg_receive_timeout(&m, 150*1000); // 150 ms must be enough
        
        if(res < 0 || m.content.value != 'A'){
            // either timeout has expired or we received something wrong
            // this turns the led OFF, note that its polarity is inverted on nRF52DK
            gpio_set(LED0_PIN);
        } else {
            gpio_clear(LED0_PIN);
        }
    }

    /* should be never reached */
    return 0;
}
