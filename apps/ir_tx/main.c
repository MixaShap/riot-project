#include <stdio.h>

#include "periph/gpio.h"
#include "periph/pwm.h"
#include "xtimer.h"

#define INPUT_PIN GPIO_PIN(0, 12)

int main(void)
{
    // PWM_DEV(0) controls LEDs, channel 0 is LED1, or P0.17 pin
    // this parameters give approx. 38 kHz frequency
    pwm_init(PWM_DEV(0), PWM_RIGHT, 38000, 210);
    
    for(uint8_t i = 0; i < pwm_channels(PWM_DEV(0)); i++){
        pwm_set(PWM_DEV(0), i, 210);
    }
    
    uint8_t byte = 'A';
    while(true){
        uint8_t byte_tx = byte;
        // NEC IR-like (no inverse byte transmission yet)
        pwm_set(PWM_DEV(0), 0, 105);
        xtimer_usleep(9000);
        pwm_set(PWM_DEV(0), 0, 0);
        xtimer_usleep(4500);
        for(int i = 0; i < 8; i++){
            pwm_set(PWM_DEV(0), 0, 105);
            xtimer_usleep(560);
            pwm_set(PWM_DEV(0), 0, 0);
            xtimer_usleep( (byte_tx & 1) ? 1700 : 560 );
            byte_tx >>= 1;
        }
        pwm_set(PWM_DEV(0), 0, 105);
        xtimer_usleep(560);
        pwm_set(PWM_DEV(0), 0, 0);
        
        // pause for 100 ms
        xtimer_msleep(100);
    }

    /* should be never reached */
    return 0;
}
